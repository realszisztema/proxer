<?php


namespace azbuco\proxer;


use azbuco\proxer\models\Cabinet;
use azbuco\proxer\models\Door;
use azbuco\proxer\models\Position;
use JsonMapper\Middleware\Rename\Rename;
use ReflectionClass;

class RenameFactory
{
    /**
     * Returns a preconfigured Rename object
     * @return Rename
     * @throws \ReflectionException
     */
    public static function create() {
        $rename = new Rename();
        $mappings = [];

        array_push($mappings, ...self::fetchMappings(Cabinet::class));
        array_push($mappings, ...self::fetchMappings(Door::class));
        array_push($mappings, ...self::fetchMappings(Position::class));

        foreach($mappings as $mapping) {
            $rename->addMapping(...$mapping);
        }

        return $rename;
    }

    /**
     * Returns the maping configurations, parsed from the class annotations
     * @param $class
     * @return array
     * @throws \ReflectionException
     */
    protected static function fetchMappings($class) {
        $reflection = new ReflectionClass($class);
        $props = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE);
        $mappings = [];
        foreach($props as $prop) {
            $comment = $prop->getDocComment();
            preg_match_all('/@map\s+(?<map>\S+)/m', $comment, $matches);
            if (!empty($matches['map'])) {
                $mapFrom = $matches['map'][count($matches['map'])-1];
                $mapTo = $prop->getName();
                $mappings[] = [$class, $mapFrom, $mapTo];
            }
        }
        return $mappings;
    }
}
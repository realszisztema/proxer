<?php

namespace azbuco\proxer\models;

class Door
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     * @map LockClosed
     */
    private $isLockClosed;

    /**
     * @var int
     * @map DoorClosed
     */
    private $isDoorClosed;

    /**
     * @var string
     * @map LastChangeTime
     */
    private $lastChangeTime;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIsLockClosed(): int
    {
        return $this->isLockClosed;
    }

    /**
     * @return bool
     */
    public function isLockClosed(): bool
    {
        return (bool)$this->getIsLockClosed();
    }

    /**
     * @param int $isLockClosed
     */
    public function setIsLockClosed(int $isLockClosed)
    {
        $this->isLockClosed = $isLockClosed;
    }

    /**
     * @return int
     */
    public function getIsDoorClosed(): int
    {
        return $this->isDoorClosed;
    }

    public function isDoorClosed(): bool
    {
        return (bool)$this->getIsDoorClosed();
    }

    /**
     * @param int $isDoorClosed
     */
    public function setIsDoorClosed(int $isDoorClosed)
    {
        $this->isDoorClosed = $isDoorClosed;
    }

    /**
     * @return string
     */
    public function getLastChangeTime(): string
    {
        return $this->lastChangeTime;
    }

    /**
     * @param string $lastChangeTime
     */
    public function setLastChangeTime(string $lastChangeTime)
    {
        $this->lastChangeTime = $lastChangeTime;
    }


}
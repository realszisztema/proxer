<?php

namespace azbuco\proxer\models;

class Cabinet
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     * @map Connected
     */
    private $isConnected;

    /**
     * @var int
     * @map FireAlarm
     */
    private $hasFireAlarm;

    /**
     * @var int
     * @map TamperAlarm
     */
    private $hasTamperAlarm;

    /**
     * @var int
     * @map PositionCnt
     */
    private $positionCount;

    /**
     * @var int
     * @map DoorCnt
     */
    private $doorCount;

    /**
     * @var int
     * @map LastReadRFID
     */
    private $lastReadRfid;

    /**
     * @var string
     * @map LastReadRFID64
     */
    private $lastReadRfid64;

    /**
     * @var string
     * @map LastReadChangeTime
     */
    private $lastReadChangeTime;

    /**
     * @var int
     * @map OpeningTimeoutSecCnt
     */
    private $autoCloseTimeout;

    /**
     * @var Door[]
     * @map Doors
     */
    private $doors;

    /**
     * @var Position[]
     * @map Positions
     */
    private $positions;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIsConnected(): int
    {
        return $this->isConnected;
    }

    /**
     * @return bool
     */
    public function isConnected(): bool
    {
        return (bool)$this->getIsConnected();
    }

    /**
     * @param int $isConnected
     */
    public function setIsConnected(int $isConnected)
    {
        $this->isConnected = $isConnected;
    }

    /**
     * @return int
     */
    public function getHasFireAlarm(): int
    {
        return $this->hasFireAlarm;
    }

    /**
     * @return bool
     */
    public function hasFireAlarm(): bool
    {
        return (bool)$this->getHasFireAlarm();
    }

    /**
     * @param int $hasFireAlarm
     */
    public function setHasFireAlarm(int $hasFireAlarm)
    {
        $this->hasFireAlarm = $hasFireAlarm;
    }

    /**
     * @return int
     */
    public function getHasTamperAlarm(): int
    {
        return $this->hasTamperAlarm;
    }

    public function hasTamperAlarm(): bool
    {
        return (bool)$this->getHasTamperAlarm();
    }

    /**
     * @param int $hasTamperAlarm
     */
    public function setHasTamperAlarm(int $hasTamperAlarm)
    {
        $this->hasTamperAlarm = $hasTamperAlarm;
    }

    /**
     * @return int
     */
    public function getPositionCount(): int
    {
        return $this->positionCount;
    }

    /**
     * @param int $positionCount
     */
    public function setPositionCount(int $positionCount)
    {
        $this->positionCount = $positionCount;
    }

    /**
     * @return int
     */
    public function getDoorCount(): int
    {
        return $this->doorCount;
    }

    /**
     * @param int $doorCount
     */
    public function setDoorCount(int $doorCount)
    {
        $this->doorCount = $doorCount;
    }

    /**
     * @return int
     */
    public function getLastReadRfid(): int
    {
        return $this->lastReadRfid;
    }

    /**
     * @param int $lastReadRfid
     */
    public function setLastReadRfid(int $lastReadRfid)
    {
        $this->lastReadRfid = $lastReadRfid;
    }

    /**
     * @return string
     */
    public function getLastReadRfid64(): string
    {
        return $this->lastReadRfid64;
    }

    /**
     * @param string $lastReadRfid64
     */
    public function setLastReadRfid64(string $lastReadRfid64)
    {
        $this->lastReadRfid64 = $lastReadRfid64;
    }

    /**
     * @return string
     */
    public function getLastReadChangeTime(): string
    {
        return $this->lastReadChangeTime;
    }

    /**
     * @param string $lastReadChangeTime
     */
    public function setLastReadChangeTime(string $lastReadChangeTime)
    {
        $this->lastReadChangeTime = $lastReadChangeTime;
    }

    /**
     * @return int
     */
    public function getAutoCloseTimeout(): int
    {
        return $this->autoCloseTimeout;
    }

    /**
     * @param int $autoCloseTimeout
     */
    public function setAutoCloseTimeout(int $autoCloseTimeout)
    {
        $this->autoCloseTimeout = $autoCloseTimeout;
    }

    /**
     * @return Door[]
     */
    public function getDoors(): array
    {
        return $this->doors;
    }

    /**
     * @param Door[] $doors
     */
    public function setDoors(array $doors)
    {
        $this->doors = $doors;
    }

    /**
     * @return Position[]
     */
    public function getPositions(): array
    {
        return $this->positions;
    }

    /**
     * @param Position[] $positions
     */
    public function setPositions(array $positions)
    {
        $this->positions = $positions;
    }


}
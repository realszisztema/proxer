<?php

namespace azbuco\proxer\models;

class Position
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     * @map Closed
     */
    private $isClosed;

    /**
     * @var int
     * @map TagRFID
     */
    private $tagRfid;

    /**
     * @var int
     * @map LastChangeTime
     */
    private $lastChangeTime;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIsClosed(): int
    {
        return $this->isClosed;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return (bool)$this->getIsClosed();
    }

    /**
     * @param int $isClosed
     */
    public function setIsClosed(int $isClosed)
    {
        $this->isClosed = $isClosed;
    }

    /**
     * @return int
     */
    public function getTagRfid(): int
    {
        return $this->tagRfid;
    }

    /**
     * @param int $tagRfid
     */
    public function setTagRfid(int $tagRfid)
    {
        $this->tagRfid = $tagRfid;
    }

    /**
     * @return int
     */
    public function getLastChangeTime(): int
    {
        return $this->lastChangeTime;
    }

    /**
     * @param int $lastChangeTime
     */
    public function setLastChangeTime(int $lastChangeTime)
    {
        $this->lastChangeTime = $lastChangeTime;
    }

}
<?php

namespace azbuco\proxer;

/**
 * Class Config
 * @package azbuco\proxer
 */
class Config
{
    public $requestOptions = [];

    public function __construct($baseUrl, $requestOptions = [])
    {
        $this->requestOptions = $requestOptions;
        $this->requestOptions['base_uri'] = $baseUrl;
    }

}
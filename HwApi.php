<?php


namespace azbuco\proxer;


use azbuco\proxer\models\Cabinet;
use GuzzleHttp\Client;
use JsonMapper\JsonMapperFactory;
use yii\helpers\Json;

class HwApi
{
    const CMD_OPEN = 'CmdOpen';
    const CMD_CLOSE = 'CmdClose';
    const CMD_BEEP = 'CmdBeep';
    const CMD_BLINK = 'CmdBlink';
    const CMD_BEEP_AND_BLINK = 'CmdBeepBlink';
    const CMD_RESET = 'CmdReset';
    const CMD_LOCK_OPEN = 'CmdLockOpen';
    const CMD_LOCK_CLOSE = 'CmdLockClose';

    const EVENT_STATUS = 'status';

    protected $_config;
    protected $_response;

    public function __construct(Config $config)
    {
        $this->_config = $config;
    }

    /**
     * @param $cabinetId
     * @return Cabinet|false
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getStatus($cabinetId)  {
        $client = $this->createClient();
        $url = "Cabinets";

        $this->_response = $client->get($url);
        if ($this->_response->getStatusCode() === 200) {
            $json = $this->_response->getBody()->getContents();
            $data = json_decode($json);

            $cabinets = [];
            foreach($data as $item) {
                $mapper = (new JsonMapperFactory())->bestFit();
                $cabinet = new Cabinet();
                $mapper->unshift(RenameFactory::create());
                $mapper->mapObject($item, $cabinet);
                if ($cabinet->getId() === $cabinetId) {
                    return $cabinet;
                }
            }
        }

        return false;
    }

    public function openPosition($cabinetId, $positionId): bool
    {
        return $this->sendPositionCommand(self::CMD_OPEN, $cabinetId, $positionId);
    }


    protected function createClient(): Client
    {
        return new Client($this->_config->requestOptions);
    }

    public function closePosition($cabinetId, $positionId): bool
    {
        return $this->sendPositionCommand(self::CMD_CLOSE, $cabinetId, $positionId);
    }

    public function blinkPosition($cabinetId, $positionId): bool
    {
        return $this->sendPositionCommand(self::CMD_BLINK, $cabinetId, $positionId);
    }

    public function beepPosition($cabinetId, $positionId): bool
    {
        return $this->sendPositionCommand(self::CMD_BEEP, $cabinetId, $positionId);
    }

    public function highlightPosition($cabinetId, $positionId): bool
    {
        return $this->sendPositionCommand(self::CMD_BEEP_AND_BLINK, $cabinetId, $positionId);
    }

    public function resetPosition($cabinetId, $positionId): bool
    {
        return $this->sendPositionCommand(self::CMD_RESET, $cabinetId, $positionId);
    }

    public function openDoor($cabinetId, $doorId): bool
    {
        return $this->sendDoorCommand(self::CMD_LOCK_OPEN, $cabinetId, $doorId);
    }


    public function closeDoor($cabinetId, $doorId): bool
    {
        return $this->sendDoorCommand(self::CMD_LOCK_CLOSE, $cabinetId, $doorId);
    }

    public function resetDoor($cabinetId, $doorId): bool
    {
        return $this->sendDoorCommand(self::CMD_RESET, $cabinetId, $doorId);
    }


    public function displayUrl($cabinetId, $url)
    {
        $client = $this->createClient();
        $endpoint = "Cabinets/$cabinetId/WebView/cmddisplayurl";

        $this->_response = $client->get($endpoint, [
            'body' => json_encode(['url' => $url]),
        ]);

        return $this->_response->getStatusCode() === 200;
    }

    public function findFob($id)
    {
        $client = $this->createClient();
        $url = "Cabinets";

        $this->_response = $client->get($url);
        if ($this->_response->getStatusCode() === 200) {
            $json = $this->_response->getBody()->getContents();
            $data = Json::decode($json);
            foreach ($data as $cabinet) {
                if ($cabinet['Positions'] && is_array($cabinet['Positions'])) {

                }
            }
        }

        return false;
    }

    protected function sendDoorCommand($command, $cabinetId, $doorId): bool
    {
        $client = $this->createClient();
        $url = "Cabinets/$cabinetId/Doors/$doorId/$command";

        $this->_response = $client->get($url);
        return $this->_response->getStatusCode() === 200;
    }

    protected function sendPositionCommand($command, $cabinetId, $positionId): bool
    {
        $client = $this->createClient();
        $url = "Cabinets/$cabinetId/Positions/$positionId/$command";

        $this->_response = $client->get($url);
        return $this->_response->getStatusCode() === 200;
    }
}